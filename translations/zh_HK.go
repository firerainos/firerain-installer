package translations

import (
	"gitlab.com/xiayesuifeng/go-i18n"
)

func init() {
	i18n.AddTranslation(i18n.Translation{
		"FireRain Installer":                     "FireRain 安裝器",
		"Back":                                   "返回",
		"Continue":                               "繼續",
		"Checking network...":                    "正在檢查網絡...",
		"Please enter your username or password": "請輸入用戶名或密碼",
		"Please connect to the network":          "請連接網絡",
		"Please connect available WIFI":          "請連接可用WIFI",
		"Please connect WIFI":                    "請連接WIFI",
		"Please select the installation disk":    "請選擇安裝磁盤",
		"Landing...":                             "登陸中...",
		"Username or password errors":            "用戶名或密碼錯誤",
		"Login failed":                           "登陸失敗",
		"Loading data...":                        "加載數據中...",
		"Login required to open":                 "需登錄開啟",
		"Reboot":                                 "重啟",
		"Installation failed\nError:":            "安裝失敗\n錯誤:",
		"Username":                               "用戶名",
		"Password":                               "密碼",
		"Skip":                                   "跳過",
		"Login":                                  "登錄",
		"Select mode":                            "選擇模式",
		"Simple mode":                            "簡單模式",
		"Advanced mode":                          "高級模式",
		"Successful installation":                "安裝成功",
		"Select the desktop environment to install": "選擇要安裝的桌面環境",
		"Select the browser to install":             "選擇您要安裝的瀏覽器",
	}, "zh_HK")
}
