package translations

import (
	"gitlab.com/xiayesuifeng/go-i18n"
)

func init() {
	i18n.AddTranslation(i18n.Translation{
		"FireRain Installer":                     "FireRain Инсталлятор",
		"Back":                                   "Назад",
		"Continue":                               "Продолжать",
		"Checking network...":                    "Проверяет сеть...",
		"Please enter your username or password": "Вводите логин или пароль",
		"Please connect to the network":          "Подключитесь к Интернету",
		"Please connect available WIFI":          "Подключитесь к доступному WIFI",
		"Please connect WIFI":                    "Подключитесь к WIFI",
		"Please select the installation disk":    "Выберите диск",
		"Landing...":                             "Погрузка...",
		"Username or password errors":            "Неправильный логин или пароль",
		"Login failed":                           "Вход не удалось",
		"Loading data...":                        "Загрузка данных...",
		"Login required to open":                 "Требуется войти чтобы открыть",
		"Reboot":                                 "Перезагрузка",
		"Installation failed\nError:":            "Установка неудавшиеся\nОшибка:",
		"Username":                               "Логин",
		"Password":                               "Пароль",
		"Skip":                                   "Пропустить",
		"Login":                                  "Логин",
		"Select mode":                            "Выберите мод",
		"Simple mode":                            "Простой мод",
		"Advanced mode":                          "Передовый мод",
		"Successful installation":                "Успешная установка",
		"Select the desktop environment to install": "Выберите среды рабочего стола чтобы установить",
		"Select the browser to install":             "Выберите браузер чтобы установить",
	}, "ru_RU")
}
