package translations

import (
	"gitlab.com/xiayesuifeng/go-i18n"
)

func init() {
	i18n.AddTranslation(i18n.Translation{
		"FireRain Installer":                     "FireRain 安装器",
		"Back":                                   "返回",
		"Continue":                               "继续",
		"Checking network...":                    "正在检查网络...",
		"Please enter your username or password": "请输入用户名或密码",
		"Please connect to the network":          "请连接网络",
		"Please connect available WIFI":          "请连接可用WIFI",
		"Please connect WIFI":                    "请连接WIFI",
		"Please select the installation disk":    "请选择安装磁盘",
		"Landing...":                             "登陆中...",
		"Username or password errors":            "用户名或密码错误",
		"Login failed":                           "登陆失败",
		"Loading data...":                        "加载数据中...",
		"Login required to open":                 "需登录开启",
		"Reboot":                                 "重启",
		"Installation failed\nError:":            "安装失败\n错误:",
		"Username":                               "用户名",
		"Password":                               "密码",
		"Skip":                                   "跳过",
		"Login":                                  "登录",
		"Select mode":                            "选择模式",
		"Simple mode":                            "简单模式",
		"Advanced mode":                          "高级模式",
		"Successful installation":                "安装成功",
		"Select the desktop environment to install": "选择要安装的桌面环境",
		"Select the browser to install":             "选择要安装的浏览器",
	}, "zh_CN")
}
