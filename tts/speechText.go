package tts

type SpeechLanguage int

const (
	CmnCn SpeechLanguage = iota
	CmnTw
	EnUs
	EnGb
	JaJp
	RuRu
	YueHk
)

var SpeechLanguageName = map[SpeechLanguage]string{
	0: "cmn-CN",
	1: "cmn-TW",
	2: "en-US",
	3: "en-GB",
	4: "ja-JP",
	5: "ru-RU",
	6: "yue-HK",
}

type SpeechTextKey string

const (
	AccountPageText     SpeechTextKey = "accountPageText"
	ModePageText        SpeechTextKey = "modePageText"
	ModePageNoLoginText SpeechTextKey = "modePageNoLoginText"
	PartitionPageText   SpeechTextKey = "partitionPageText"
	SelectDEPageText    SpeechTextKey = "selectDEPageText"
	BrowserPageText     SpeechTextKey = "browserPageText"
	InfoPageText        SpeechTextKey = "infoPageText"
)

type SpeechText map[SpeechTextKey]string

var speechTexts map[SpeechLanguage]SpeechText
var speechLanguage SpeechLanguage

func init() {
	speechTexts = make(map[SpeechLanguage]SpeechText)

	speechTexts[CmnCn] = SpeechText{
		AccountPageText:     "网络已连接，现在您可以登录你的帐号来解锁高级模式或者跳过登录",
		ModePageText:        "现在您可以选择简单模式或者高级模式来进行安装，其中高级模式提供自由添加预安装软件包功能",
		ModePageNoLoginText: "现在您可以选择简单模式来进行安装，高级模式则需要先登录帐号",
		PartitionPageText:   "现在请选择安装分区，如没有显示任何分区请打开分区管理器新建分区，并且文件系统必须是 Btrfs",
		SelectDEPageText:    "现在请选择您要安装的桌面环境",
		BrowserPageText:     "现在请选择您要安装的浏览器",
		InfoPageText:        "现在请您检查以下安装信息，如确认无误请点继续来进行安装",
	}

	speechTexts[CmnTw] = SpeechText{
		AccountPageText:     "網絡已連接，現在您可以登錄你的帳號來解鎖高級模式或者跳過登錄",
		ModePageText:        "現在您可以選擇簡單模式或者高級模式來進行安裝，其中高級模式提供自由添加預安裝軟件包功能",
		ModePageNoLoginText: "現在您可以選擇簡單模式來進行安裝，高級模式則需要先登錄帳號",
		PartitionPageText:   "現在請選擇安裝分區，如沒有顯示任何分區請打開分區管理器新建分區，並且文件系統必須是 Btrfs",
		SelectDEPageText:    "現在請選擇您要安裝的桌面環境",
		BrowserPageText:     "現在請選擇您要安裝的瀏覽器",
		InfoPageText:        "現在請您檢查以下安裝信息，如確認無誤請點繼續來進行安裝",
	}

	speechTexts[EnUs] = SpeechText{
		AccountPageText:     "The network is connected, now you can log in to your account to unlock the advanced mode or skip the login",
		ModePageText:        "Now you can choose simple mode or advanced mode to install, among which the advanced mode provides the function of freely adding pre-installed software packages",
		ModePageNoLoginText: "Now you can choose the simple mode to install, and in the advanced mode, you need to log in to your account first",
		PartitionPageText:   "Now please select the installation partition. If no partition is displayed, please open the partition manager to create a new partition, and the file system must be Btrfs",
		SelectDEPageText:    "Now please select the desktop environment you want to install",
		BrowserPageText:     "Now please select the browser you want to install",
		InfoPageText:        "Now please check the following installation information, if you confirm it is correct, please click Continue to install",
	}

	speechTexts[YueHk] = SpeechText{
		AccountPageText:     "網絡已連接，宜家你可以登錄你嘅帳號來解鎖高級模式或者跳過登錄",
		ModePageText:        "宜家您可以選擇簡單模式或者高級模式來進行安裝，其中高級模式提供自由添加預安裝軟件包功能",
		ModePageNoLoginText: "宜家您可以選擇簡單模式來進行安裝，高級模式就需要先登錄帳號",
		PartitionPageText:   "宜家請選擇安裝分區，如無顯示任何分區請打開分區管理器新建分區，而且文件系統一定要係 Btrfs",
		SelectDEPageText:    "宜家請選擇你要安裝嘅桌面環境",
		BrowserPageText:     "宜家請選擇你要安裝嘅瀏覽器",
		InfoPageText:        "宜家請你檢查以下安裝信息，如確認無誤請點繼續來進行安裝",
	}

	speechTexts[RuRu] = SpeechText{
	AccountPageText:     "Сеть подключена, теперь Вы можете войти в свою учетную запись, чтобы разблокировать расширенный режим или пропустить вход",
	ModePageText:        "Теперь Вы можете выбрать простой режим или расширенный режим для установки, среди которых расширенный режим предоставляет функцию свободного добавления предустановленных пакетов программного обеспечения",
	ModePageNoLoginText: "Теперь Вы можете выбрать простой режим для установки, а в расширенном режиме Вам нужно сначала войти в свою учетную запись",
	PartitionPageText:   "Теперь выберите установочный раздел. Если раздел не отображается, откройте диспетчер разделов, чтобы создать новый раздел, и файловая система должна быть Btrfs",
	SelectDEPageText:    "Теперь выберите среду рабочего стола, которую Вы хотите установить",
	BrowserPageText:     "Теперь выберите браузер, который Вы хотите установить",
	InfoPageText:        "Теперь проверьте следующую информацию об установке. Если Вы подтвердите, что она верна, нажмите «Продолжить» для установки.",
	}
}

func SetSpeechLanguage(language SpeechLanguage) {
	speechLanguage = language
}

func GetSpeechText(key SpeechTextKey) string {
	if text, exist := speechTexts[speechLanguage][key]; exist {
		return text
	} else if text, exist = speechTexts[EnUs][key]; exist {
		return text
	}

	return ""
}
