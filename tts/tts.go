package tts

import (
	"errors"
	"github.com/hajimehoshi/oto"
	"github.com/tosone/minimp3"
	"gitlab.com/firerainos/firerain-installer/api"
	"io"
	"sync"
)

var (
	otoContext   *oto.Context
	decoder      *minimp3.Decoder
	speechLocker *sync.Mutex
)

func Speech(text, language string) error {
	body, err := api.TextToSpeech(text, language)
	if err != nil {
		return err
	}
	defer body.Close()

	if decoder != nil {
		decoder.Close()
		decoder = nil
	}
	if speechLocker == nil {
		speechLocker = new(sync.Mutex)
	}
	speechLocker.Lock()
	defer speechLocker.Unlock()
	if decoder, err = minimp3.NewDecoder(body); err != nil {
		return err
	}
	<-decoder.Started()
	defer func() {
		if decoder != nil {
			decoder.Close()
			decoder = nil
		}
	}()

	if otoContext == nil {
		return errors.New("oto context is nil")
	}

	player := otoContext.NewPlayer()
	defer player.Close()

	_, err = io.Copy(player, decoder)
	return err
}

func SpeechWithKey(key SpeechTextKey) error {
	return Speech(GetSpeechText(key), SpeechLanguageName[speechLanguage])
}

func InitTTS() error {
	ipApi, err := api.GetIpApi()
	if err != nil {
		return err
	}

	switch ipApi.CountryCode {
	case "GB":
		SetSpeechLanguage(EnGb)
	case "CN":
		switch ipApi.Region {
		case "GD":
			fallthrough
		case "HI":
			fallthrough
		case "MO":
			SetSpeechLanguage(YueHk)
		default:
			SetSpeechLanguage(CmnCn)
		}
	case "TW":
		SetSpeechLanguage(CmnTw)
	case "JP":
		SetSpeechLanguage(JaJp)
	case "HK":
		SetSpeechLanguage(YueHk)
	case "RU":
		SetSpeechLanguage(RuRu)
	default:
		SetSpeechLanguage(EnUs)
	}

	if otoContext == nil {
		otoContext, err = oto.NewContext(24000, 1, 2, 4096)
		if err != nil {
			return err
		}
	}

	return nil
}
