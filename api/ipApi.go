package api

import (
	"encoding/json"
	"net/http"
)

type IpApi struct {
	Query       string `json:"query"`
	Country     string `json:"country"`
	CountryCode string `json:"countryCode"`
	Region      string `json:"region"`
	City        string `json:"city"`
	Timezone    string `json:"timezone"`
}

func GetIpApi() (*IpApi, error) {
	resp, err := http.Get("http://ip-api.com/json")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	ipApi := &IpApi{}

	err = json.NewDecoder(resp.Body).Decode(ipApi)

	return ipApi, err
}
