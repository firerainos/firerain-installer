package api

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

func TextToSpeech(text, language string) (io.ReadCloser, error) {
	client := &http.Client{}

	urlStr := fmt.Sprintf("https://firerain.me/api/textToSpeech/%s?text=%s", language, url.QueryEscape(text))
	req, err := http.NewRequest("GET", urlStr, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", "FireRain Installer/1.0.0")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("request textToSpeech fail, status code: " + resp.Status)
	}

	return resp.Body, nil
}
