package ui

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/multimedia"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/firerainos/firerain-installer/api"
	"gitlab.com/firerainos/firerain-installer/config"
	"gitlab.com/firerainos/firerain-installer/core/installer"
	"gitlab.com/firerainos/firerain-installer/styles"
	"gitlab.com/firerainos/firerain-installer/tts"
	"gitlab.com/firerainos/firerain-installer/ui/page"
	"gitlab.com/xiayesuifeng/go-i18n"
	"log"
	"os/exec"
	"strings"
)

type MainFrame struct {
	*widgets.QFrame

	welcomePage            *page.WelcomePage
	networkPage            *page.NetworkPage
	accountPage            *page.AccountPage
	modePage               *page.ModePage
	partitionPage          *page.PartitionPage
	selectDEPage           *page.SelectDEPage
	browserPage            *page.BrowserPage
	additionalSoftwarePage *page.AdditionalSoftwarePage
	infoPage               *page.InfoPage
	installPage            *page.InstallPage
	endPage                *page.EndPage

	backButton, nextButton *widgets.QPushButton

	stackLayout *widgets.QStackedLayout

	account *api.Account

	mediaPlayer *multimedia.QMediaPlayer
}

func NewMainFrame(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *MainFrame {
	frame := &MainFrame{QFrame: widgets.NewQFrame(parent, fo), account: api.NewAccount()}

	frame.init()
	frame.initPlayer()
	frame.initConnect()

	return frame
}

func (m *MainFrame) init() {
	vboxLayout := widgets.NewQVBoxLayout()
	vboxLayout.SetContentsMargins(0, 0, 0, 0)

	m.stackLayout = widgets.NewQStackedLayout()

	m.welcomePage = page.NewWelcomePage(m, 0)
	m.networkPage = page.NewNetworkPage(m, 0)
	m.accountPage = page.NewAccountPage(m, 0)
	m.modePage = page.NewModePage(m, 0)
	m.partitionPage = page.NewPartitionPage(m, 0)
	m.selectDEPage = page.NewSelectDEPage(m, 0)
	m.browserPage = page.NewBrowserPage(m, 0)
	m.additionalSoftwarePage = page.NewAdditionalSoftwarePage(m.account, m, 0)
	m.infoPage = page.NewInfoPage(m, 0)
	m.installPage = page.NewInstallPage(m, 0)
	m.endPage = page.NewEndPage(m, 0)

	m.backButton = widgets.NewQPushButton2(i18n.Tr("Back"), m)
	m.nextButton = widgets.NewQPushButton2(i18n.Tr("Continue"), m)

	m.backButton.SetMinimumWidth(60)
	m.nextButton.SetMinimumWidth(60)

	m.backButton.SetStyleSheet(styles.BackButton)
	m.nextButton.SetStyleSheet(styles.NextButton)

	m.backButton.SetVisible(false)

	hboxLayout := widgets.NewQHBoxLayout()
	hboxLayout.SetSpacing(40)

	hboxLayout.AddStretch(1)
	hboxLayout.AddWidget(m.backButton, 0, core.Qt__AlignHCenter)
	hboxLayout.AddWidget(m.nextButton, 0, core.Qt__AlignHCenter)
	hboxLayout.AddStretch(1)

	m.stackLayout.AddWidget(m.welcomePage)
	m.stackLayout.AddWidget(m.networkPage)
	m.stackLayout.AddWidget(m.accountPage)
	m.stackLayout.AddWidget(m.modePage)
	m.stackLayout.AddWidget(m.partitionPage)
	m.stackLayout.AddWidget(m.selectDEPage)
	m.stackLayout.AddWidget(m.browserPage)
	m.stackLayout.AddWidget(m.additionalSoftwarePage)
	m.stackLayout.AddWidget(m.infoPage)
	m.stackLayout.AddWidget(m.installPage)
	m.stackLayout.AddWidget(m.endPage)

	vboxLayout.AddLayout(m.stackLayout, 1)
	vboxLayout.AddLayout(hboxLayout, 1)
	vboxLayout.AddSpacing(20)

	m.SetLayout(vboxLayout)
}

func (m *MainFrame) initPlayer() {
	m.mediaPlayer = multimedia.NewQMediaPlayer(m, 0)
	playList := multimedia.NewQMediaPlaylist(m)
	playList.AddMedia(multimedia.NewQMediaContent2(core.NewQUrl3("qrc:/resources/background.mp3", core.QUrl__TolerantMode)))
	playList.SetPlaybackMode(multimedia.QMediaPlaylist__CurrentItemInLoop)
	m.mediaPlayer.SetPlaylist(playList)
}

func (m *MainFrame) initConnect() {
	m.welcomePage.ConnectLanguageChange(m.LanguageChange)
	m.welcomePage.ConnectLanguageChange(m.accountPage.LanguageChange)
	m.welcomePage.ConnectLanguageChange(m.modePage.LanguageChange)
	m.welcomePage.ConnectLanguageChange(m.partitionPage.LanguageChange)
	m.welcomePage.ConnectLanguageChange(m.browserPage.LanguageChange)
	m.welcomePage.ConnectLanguageChange(m.selectDEPage.LanguageChange)
	m.welcomePage.ConnectLanguageChange(m.endPage.LanguageChange)

	m.accountPage.SkipButton.ConnectClicked(func(checked bool) {
		go tts.SpeechWithKey(tts.ModePageNoLoginText)
		m.stackLayout.SetCurrentIndex(m.stackLayout.CurrentIndex() + 1)
		m.modePage.SetAdvancedModeEnable(false)
	})

	m.stackLayout.ConnectCurrentChanged(func(index int) {
		if index == 0 {
			m.backButton.SetVisible(false)
		} else if index == m.stackLayout.Count()-2 || index == 1 {
			m.nextButton.SetVisible(false)
			m.backButton.SetVisible(false)
		} else if index > 0 && index < 9 {
			m.backButton.SetVisible(true)
		}

		switch index {
		case 1:
			m.networkPage.SetTips(i18n.Tr("Checking network..."))
			go m.checkNetwork()
		case 4:
			go tts.SpeechWithKey(tts.PartitionPageText)
		case 5:
			go tts.SpeechWithKey(tts.SelectDEPageText)
		case 6:
			go tts.SpeechWithKey(tts.BrowserPageText)
		case 8:
			go tts.SpeechWithKey(tts.InfoPageText)
		}
	})

	m.backButton.ConnectClicked(func(checked bool) {
		if m.stackLayout.CurrentIndex() == 8 || m.stackLayout.CurrentIndex() == 2 {
			m.stackLayout.SetCurrentIndex(m.stackLayout.CurrentIndex() - 2)
		} else {
			m.stackLayout.SetCurrentIndex(m.stackLayout.CurrentIndex() - 1)
		}
	})

	m.nextButton.ConnectClicked(func(checked bool) {
		index := m.stackLayout.CurrentIndex()
		switch index {
		case 1:
			m.networkPage.SetTips(i18n.Tr("Checking network..."))
			go m.checkNetwork()
			return
		case 2:
			if config.Conf.Username == "" || config.Conf.Password == "" {
				m.accountPage.SetTips(i18n.Tr("Please enter your username or password"))
				return
			}

			m.setButtonVisible(false)
			m.accountPage.SetEnableLogin(false)
			m.accountPage.SetTips(i18n.Tr("Landing..."))
			m.accountPage.Repaint()

			if err := m.account.Login(config.Conf.Username, config.Conf.Password); err != nil {
				if strings.Contains(err.Error(), "username or password errors") {
					m.accountPage.SetTips(i18n.Tr("Username or password errors"))
				} else {
					m.accountPage.SetTips(i18n.Tr("Login failed"))
				}
				m.setButtonVisible(true)
				m.accountPage.SetEnableLogin(true)
				return
			}
			m.accountPage.SetTips(i18n.Tr("Loading data..."))
			m.accountPage.Repaint()
			installer.SyncDatabase()
			m.additionalSoftwarePage.LoadData()
			m.nextButton.SetVisible(true)
			m.accountPage.SetEnableLogin(true)
			m.modePage.SetAdvancedModeEnable(true)
			m.accountPage.SetTips("")
			go tts.SpeechWithKey(tts.ModePageText)
		case 4:
			if config.Conf.InstallDev == "" {
				return
			}
		case 6:
			if config.Conf.SimpleMode {
				index++
				m.infoPage.UpdateInfo()
			} else {
				m.additionalSoftwarePage.LoadInstallList()
			}
		case 7:
			m.infoPage.UpdateInfo()
		case 8:
			m.mediaPlayer.Play()
			go m.install()
		case 10:
			m.reboot()
		}
		m.stackLayout.SetCurrentIndex(index + 1)
	})
}

func (m *MainFrame) checkNetwork() {
	cmd := exec.Command("ping", "-c", "3", "www.baidu.com")
	if err := cmd.Run(); err != nil {
		m.networkPage.ConnectNetwork()
	} else {
		log.Println("init TTS, error: ", tts.InitTTS())
		go tts.SpeechWithKey(tts.AccountPageText)
		m.stackLayout.SetCurrentIndex(2)
	}
	m.setButtonVisible(true)
}

func (m *MainFrame) install() {
	message := make(chan string)

	go func() {
		for msg := range message {
			if strings.HasPrefix(msg, "message:") {
				m.installPage.SetTips(strings.Split(msg, ":")[1])
			} else if strings.HasPrefix(msg, "action:") {
				if strings.HasSuffix(msg, "closeMessage") {
					m.installPage.SetMessageVisible()
				}
			} else {
				m.installPage.AddMessage(msg)
			}
		}
	}()

	err := installer.Install(message)
	if err != nil {
		m.endPage.SetTips(i18n.Tr("Installation failed\nError:") + err.Error())
	}

	m.stackLayout.SetCurrentIndex(9)
	m.nextButton.SetText(i18n.Tr("Reboot"))
	m.nextButton.SetVisible(true)
}

func (m *MainFrame) setButtonVisible(enable bool) {
	m.backButton.SetVisible(enable)
	m.nextButton.SetVisible(enable)
}

func (m *MainFrame) reboot() {
	exec.Command("reboot").Run()
}

func (m *MainFrame) LanguageChange() {
	m.ParentWidget().SetWindowTitle(i18n.Tr("FireRain Installer"))
	m.backButton.SetText(i18n.Tr("Back"))
	m.nextButton.SetText(i18n.Tr("Continue"))
}
