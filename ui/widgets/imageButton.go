package widgets

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/firerainos/firerain-installer/styles"
)

type ImageButton struct {
	*widgets.QPushButton

	image, title string

	titleLabel *widgets.QLabel

	size core.QSize_ITF
}

func NewImageButton(image, title string, size core.QSize_ITF, parent widgets.QWidget_ITF) *ImageButton {
	widget := widgets.NewQPushButton(parent)

	imageButton := &ImageButton{QPushButton: widget, image: image, title: title, size: size}
	imageButton.init()

	return imageButton
}

func (ptr *ImageButton) init() {
	ptr.SetFixedSize2(250, 250)

	vboxLayout := widgets.NewQVBoxLayout2(ptr)

	image := widgets.NewQLabel(ptr, 0)
	image.SetFixedSize(ptr.size)
	image.SetPixmap(gui.NewQPixmap3(ptr.image, "", 0).Scaled(ptr.size, core.Qt__KeepAspectRatioByExpanding, 0))
	image.SetAlignment(core.Qt__AlignCenter)

	ptr.titleLabel = widgets.NewQLabel2(ptr.title, ptr, 0)

	vboxLayout.AddStretch(1)
	vboxLayout.AddWidget(image, 0, core.Qt__AlignHCenter)
	vboxLayout.AddSpacing(5)
	vboxLayout.AddWidget(ptr.titleLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddStretch(1)

	ptr.SetStyleSheet(styles.ImageButton)

	ptr.SetCheckable(true)
}

func (ptr *ImageButton) SetTitle(title string) {
	ptr.title = title
	ptr.titleLabel.SetText(title)
}
