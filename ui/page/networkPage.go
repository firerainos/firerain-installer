package page

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/firerainos/firerain-installer/core/networkmanager"
	"gitlab.com/firerainos/firerain-installer/styles"
	widgets2 "gitlab.com/firerainos/firerain-installer/ui/widgets"
	"gitlab.com/xiayesuifeng/go-i18n"
)

type NetworkPage struct {
	widgets.QFrame

	tipsLabel *widgets.QLabel
	wifiList  *widgets.QListWidget

	nm *networkmanager.NetworkManager

	stopScan bool

	_ func() `constructor:"init"`

	_ func(wifiList networkmanager.WifiList) `signal:"wifiListChange"`
}

func (n *NetworkPage) init() {
	n.nm = networkmanager.NewNetworkManager()
	vboxLayout := widgets.NewQVBoxLayout2(n)

	n.tipsLabel = widgets.NewQLabel2(i18n.Tr("Checking network..."), n, 0)
	n.wifiList = widgets.NewQListWidget(n)

	n.wifiList.SetVisible(false)
	n.wifiList.SetMinimumSize2(480, 500)
	n.wifiList.SetStyleSheet(styles.WifiListWidget)

	vboxLayout.AddWidget(n.tipsLabel, 0, core.Qt__AlignCenter)
	vboxLayout.AddWidget(n.wifiList, 0, core.Qt__AlignCenter)

	n.SetLayout(vboxLayout)

	go n.scanWifi()

	n.ConnectWifiListChange(n.wifiListChange)
}

func (n *NetworkPage) scanWifi() {
	for wifiList := range n.nm.WifiList() {
		n.WifiListChange(wifiList)
	}
}

func (n *NetworkPage) wifiListChange(wifiList networkmanager.WifiList) {
	n.wifiList.Clear()

	for _, wifiInfo := range wifiList {
		item := widgets.NewQListWidgetItem(n.wifiList, 0)
		item.SetSizeHint(core.NewQSize2(460, 50))
		listItem := widgets2.NewWifiListItem(n, 0)
		listItem.SetWifiInfo(wifiInfo)
		n.wifiList.SetItemWidget(item, listItem)

		listItem.ConnectWifiListItemClicked(n.onWifiListItemClicked)
	}
}

func (n *NetworkPage) onWifiListItemClicked(ssid string, security bool, inUse bool) {
	n.nm.StopWifiScan()
	if !security {
		n.nm.ConnectWifi(ssid, "")
	} else {
		dialog := widgets.NewQInputDialog(n, 0)
		ok := false
		password := dialog.GetText(n, i18n.Tr("Prompt"), "请输入"+ssid+"的密码", widgets.QLineEdit__Password, "", &ok, 0, 0)
		if ok && password == "" {
			n.nm.WifiScan()
			return
		}
		n.nm.ConnectWifi(ssid, password)
	}
	n.nm.WifiScan()
}
func (n *NetworkPage) SetTips(tips string) {
	n.tipsLabel.SetText(tips)
}

func (n *NetworkPage) ConnectNetwork() {
	if n.nm.CheckHasWifi() {
		n.nm.SetWifiStatus(true)

		if !n.wifiList.IsVisible() {
			n.nm.WifiScan()
			n.tipsLabel.SetText(i18n.Tr("Please connect WIFI"))
		} else {
			n.tipsLabel.SetText(i18n.Tr("Please connect available WIFI"))
		}
	} else {
		n.tipsLabel.SetText(i18n.Tr("Please connect to the network"))
	}
	n.wifiList.SetVisible(true)
}
