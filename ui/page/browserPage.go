package page

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/firerainos/firerain-installer/config"
	_ "gitlab.com/firerainos/firerain-installer/resources"
	"gitlab.com/firerainos/firerain-installer/styles"
	"gitlab.com/xiayesuifeng/go-i18n"
)

type BrowserPage struct {
	*widgets.QFrame

	browserListWidget *widgets.QListWidget

	welcomeLabel *widgets.QLabel

	browserName []string
}

func NewBrowserPage(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *BrowserPage {
	frame := widgets.NewQFrame(parent, fo)

	page := &BrowserPage{QFrame: frame}
	page.init()
	page.initConnect()

	return page
}

func (s *BrowserPage) init() {
	vboxLayout := widgets.NewQVBoxLayout2(s)

	s.welcomeLabel = widgets.NewQLabel2(i18n.Tr("Select the browser to install"), s, 0)
	s.browserListWidget = widgets.NewQListWidget(s)

	s.browserListWidget.SetMinimumSize2(620, 210)
	s.browserListWidget.SetViewMode(widgets.QListView__IconMode)
	s.browserListWidget.SetFlow(widgets.QListView__LeftToRight)
	s.browserListWidget.SetMovement(widgets.QListView__Static)
	s.browserListWidget.SetVerticalScrollBarPolicy(core.Qt__ScrollBarAlwaysOff)
	s.browserListWidget.SetIconSize(core.NewQSize2(70, 70))
	s.browserListWidget.SetSpacing(40)
	s.browserListWidget.SetStyleSheet(styles.BrowserList)

	widgets.NewQListWidgetItem3(gui.NewQIcon5(":/resources/browser/firefox.png"), "Firefox", s.browserListWidget, 0).SetSizeHint(core.NewQSize2(150, 150))
	widgets.NewQListWidgetItem3(gui.NewQIcon5(":/resources/browser/chromium.png"), "Chromium", s.browserListWidget, 0).SetSizeHint(core.NewQSize2(150, 150))
	widgets.NewQListWidgetItem3(gui.NewQIcon5(":/resources/browser/chrome.png"), "Chrome", s.browserListWidget, 0).SetSizeHint(core.NewQSize2(150, 150))

	s.browserListWidget.SetCurrentRow(0)

	vboxLayout.AddStretch(1)
	vboxLayout.AddWidget(s.welcomeLabel, 0, core.Qt__AlignCenter)
	vboxLayout.AddStretch(1)
	vboxLayout.AddWidget(s.browserListWidget, 0, core.Qt__AlignCenter)
	vboxLayout.AddStretch(1)

	s.SetLayout(vboxLayout)
}

func (s *BrowserPage) initConnect() {
	s.browserListWidget.ConnectCurrentTextChanged(func(currentText string) {
		for _, pkg := range s.browserName {
			config.Conf.RemovePackage(pkg)
		}

		switch currentText {
		case "Firefox":
			config.Conf.Browser = "firefox"
			s.browserName = []string{"firefox"}
			switch i18n.GetLanguage() {
			case "zh_CN":
				s.browserName = append(s.browserName, "firefox-i18n-zh-cn")
			case "ru_RU":
				s.browserName = append(s.browserName, "firefox-i18n-ru")
			}
		case "Chromium":
			config.Conf.Browser = "chromium"
			s.browserName = []string{"chromium"}
		case "Chrome":
			config.Conf.Browser = "google-chrome"
			s.browserName = []string{"google-chrome"}
		}

		for _, pkg := range s.browserName {
			config.Conf.AddPackage(pkg)
		}
	})

	s.browserListWidget.CurrentTextChanged("Firefox")
}

func (s *BrowserPage) LanguageChange() {
	s.welcomeLabel.SetText(i18n.Tr("Select the browser to install"))
}
