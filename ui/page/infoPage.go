package page

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/firerainos/firerain-installer/config"
	"strings"
)

type InfoPage struct {
	*widgets.QFrame

	tipsLabel *widgets.QLabel

	deInfoLabel, partitionInfoLabel, packageInfoLabel, efiInfoLabel *widgets.QLabel
	efiInfoComboBox                                                 *widgets.QComboBox
}

func NewInfoPage(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *InfoPage {
	frame := widgets.NewQFrame(parent, fo)

	page := &InfoPage{QFrame: frame}
	page.init()

	return page
}

func (page *InfoPage) init() {
	vboxLayout := widgets.NewQVBoxLayout2(page)

	page.tipsLabel = widgets.NewQLabel2("安装信息", page, 0)

	deLabel := widgets.NewQLabel2("选择的桌面环境", page, 0)
	page.deInfoLabel = widgets.NewQLabel(page, 0)

	partitionLabel := widgets.NewQLabel2("FireRain安装位置", page, 0)
	page.partitionInfoLabel = widgets.NewQLabel(page, 0)

	efiLabel := widgets.NewQLabel2("EFI 分区", page, 0)
	page.efiInfoComboBox = widgets.NewQComboBox(page)
	page.efiInfoLabel = widgets.NewQLabel2("none", page, 0)

	packageLabel := widgets.NewQLabel2("将安装以下包或组", page, 0)
	page.packageInfoLabel = widgets.NewQLabel(page, 0)

	page.packageInfoLabel.SetWordWrap(true)

	vboxLayout.AddStretch(1)
	vboxLayout.AddWidget(page.tipsLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddStretch(1)
	vboxLayout.AddWidget(deLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddWidget(page.deInfoLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddSpacing(30)
	vboxLayout.AddWidget(partitionLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddWidget(page.partitionInfoLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddSpacing(30)
	vboxLayout.AddWidget(efiLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddWidget(page.efiInfoComboBox, 0, core.Qt__AlignHCenter)
	vboxLayout.AddWidget(page.efiInfoLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddSpacing(30)
	vboxLayout.AddWidget(packageLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddWidget(page.packageInfoLabel, 1, core.Qt__AlignHCenter)
	vboxLayout.AddStretch(1)

	page.efiInfoComboBox.ConnectCurrentTextChanged(func(text string) {
		if config.Conf.IsUEFI {
			config.Conf.SetEFIDev(text)
		}
	})

	page.SetLayout(vboxLayout)
}

func (page *InfoPage) UpdateInfo() {
	de := ""
	switch config.Conf.DEApplication {
	case "kde-applications":
		de = "KDE"
	case "deepin-extra":
		de = "DDE"
	case "gnome-extra":
		if config.Conf.SearchPackage("cinnamon") {
			de = "Cinnamon"
		} else {
			de = "GNOME"
		}
	}

	page.efiInfoComboBox.SetVisible(config.Conf.IsUEFI)
	page.efiInfoLabel.SetVisible(!config.Conf.IsUEFI)
	if config.Conf.IsUEFI {
		page.efiInfoComboBox.Clear()
		page.efiInfoComboBox.AddItems(config.Conf.AllEFIDev)
		page.efiInfoComboBox.SetCurrentTextDefault(config.Conf.EFIDev)
	}

	page.deInfoLabel.SetText(de)
	page.partitionInfoLabel.SetText(config.Conf.InstallDev)
	page.packageInfoLabel.SetText(strings.Join(config.Conf.Package.PkgList, " "))
}
