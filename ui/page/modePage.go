package page

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/firerainos/firerain-installer/config"
	widgets2 "gitlab.com/firerainos/firerain-installer/ui/widgets"
	"gitlab.com/xiayesuifeng/go-i18n"
)

type ModePage struct {
	*widgets.QFrame

	tipsLabel *widgets.QLabel

	simpleButton, advancedButton *widgets2.ImageButton
}

func NewModePage(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *ModePage {
	frame := widgets.NewQFrame(parent, fo)

	page := &ModePage{QFrame: frame}
	page.init()

	return page
}

func (page *ModePage) init() {
	vboxLayout := widgets.NewQVBoxLayout2(page)

	page.tipsLabel = widgets.NewQLabel2(i18n.Tr("Select mode"), page, 0)

	modeLayout := widgets.NewQHBoxLayout2(page)

	page.simpleButton = widgets2.NewImageButton(":/resources/simple_install.png", i18n.Tr("Simple mode"), core.NewQSize2(200, 200), page)
	page.advancedButton = widgets2.NewImageButton(":/resources/advanced_install.png", i18n.Tr("Advanced mode"), core.NewQSize2(200, 200), page)

	page.simpleButton.SetChecked(true)

	modeLayout.AddStretch(1)
	modeLayout.AddWidget(page.simpleButton, 0, core.Qt__AlignCenter)
	modeLayout.AddSpacing(50)
	modeLayout.AddWidget(page.advancedButton, 0, core.Qt__AlignCenter)
	modeLayout.AddStretch(1)

	vboxLayout.AddStretch(1)
	vboxLayout.AddWidget(page.tipsLabel, 0, core.Qt__AlignHCenter)
	vboxLayout.AddStretch(1)
	vboxLayout.AddLayout(modeLayout, 0)
	vboxLayout.AddStretch(1)

	page.SetLayout(vboxLayout)

	page.simpleButton.ConnectClicked(func(checked bool) {
		config.Conf.SimpleMode = true
		page.simpleButton.SetChecked(true)
		page.advancedButton.SetChecked(false)
	})

	page.advancedButton.ConnectClicked(func(checked bool) {
		config.Conf.SimpleMode = false
		page.simpleButton.SetChecked(false)
		page.advancedButton.SetChecked(true)
	})
}

func (page *ModePage) SetTips(tips string) {
	page.tipsLabel.SetText(tips)
}

func (page *ModePage) SetAdvancedModeEnable(enable bool) {
	page.advancedButton.SetEnabled(enable)
	if enable {
		page.advancedButton.SetText("")
	} else {
		page.advancedButton.SetText(i18n.Tr("Login required to open"))
	}
}

func (page *ModePage) LanguageChange() {
	page.tipsLabel.SetText(i18n.Tr("Select mode"))
	page.simpleButton.SetTitle(i18n.Tr("Simple mode"))
	page.advancedButton.SetTitle(i18n.Tr("Advanced mode"))
}
