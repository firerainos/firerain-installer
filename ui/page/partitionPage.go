package page

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/firerainos/firerain-installer/config"
	widgets2 "gitlab.com/firerainos/firerain-installer/ui/widgets"
	"gitlab.com/xiayesuifeng/go-i18n"
)

type PartitionPage struct {
	*widgets.QFrame

	tipLabel *widgets.QLabel

	partitionList *widgets2.PartitionList
}

func NewPartitionPage(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *PartitionPage {
	frame := widgets.NewQFrame(parent, fo)

	page := &PartitionPage{QFrame: frame}
	page.init()
	page.initConnect()

	return page
}

func (p *PartitionPage) init() {
	vboxLayout := widgets.NewQVBoxLayout2(p)

	logoLabel := widgets.NewQLabel(p, 0)
	welcomeLabel := widgets.NewQLabel2("FireRainOS", p, 0)
	p.tipLabel = widgets.NewQLabel2(i18n.Tr("Please select the installation disk"), p, 0)
	p.partitionList = widgets2.NewPartitionList(p)

	logoLabel.SetPixmap(gui.NewQPixmap3(":/resources/logo.png", "", 0).Scaled2(200, 200, core.Qt__KeepAspectRatioByExpanding, 0))
	logoLabel.SetFixedSize2(200, 200)

	vboxLayout.AddWidget(logoLabel, 0, core.Qt__AlignCenter)
	vboxLayout.AddWidget(welcomeLabel, 0, core.Qt__AlignCenter)
	vboxLayout.AddWidget(p.tipLabel, 0, core.Qt__AlignCenter)
	vboxLayout.AddWidget(p.partitionList, 0, core.Qt__AlignCenter)

	p.SetLayout(vboxLayout)
}

func (p *PartitionPage) initConnect() {
	p.partitionList.ConnectPartitionItemChange(func() {
		p.tipLabel.SetText("FireRainOS 将安装在磁盘\"" + p.partitionList.CurrendItem().DevPath() + "\"上")
		config.Conf.SetInstallDev(p.partitionList.CurrendItem().DevPath())
	})
}

func (p *PartitionPage) LanguageChange() {
	p.tipLabel.SetText(i18n.Tr("Please select the installation disk"))
}
