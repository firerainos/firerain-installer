package page

import (
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/firerainos/firerain-installer/config"
	_ "gitlab.com/firerainos/firerain-installer/resources"
	widgets2 "gitlab.com/firerainos/firerain-installer/ui/widgets"
	"gitlab.com/xiayesuifeng/go-i18n"
)

type AccountPage struct {
	*widgets.QFrame

	loginLabel, tipsLabel *widgets.QLabel

	username, password *widgets2.LineEdit

	SkipButton *widgets.QPushButton
}

func NewAccountPage(parent widgets.QWidget_ITF, fo core.Qt__WindowType) *AccountPage {
	frame := widgets.NewQFrame(parent, fo)

	accountPage := &AccountPage{QFrame: frame}
	accountPage.init()

	return accountPage
}

func (page *AccountPage) init() {
	mainLayout := widgets.NewQHBoxLayout2(page)

	vboxLayout := widgets.NewQVBoxLayout2(page)

	page.loginLabel = widgets.NewQLabel2(i18n.Tr("Login"), page, 0)

	page.tipsLabel = widgets.NewQLabel(page, 0)
	page.tipsLabel.SetMinimumWidth(400)
	page.tipsLabel.SetAlignment(core.Qt__AlignCenter)

	page.username = widgets2.NewLineEdit(":/resources/username.svg", page)
	page.password = widgets2.NewLineEdit(":/resources/password.svg", page)
	page.password.SetEchoMode(widgets.QLineEdit__Password)

	page.username.SetPlaceholderText(i18n.Tr("Username"))
	page.password.SetPlaceholderText(i18n.Tr("Password"))

	page.SkipButton = widgets.NewQPushButton2(i18n.Tr("Skip"), page)
	page.SkipButton.SetFlat(true)

	vboxLayout.AddStretch(1)
	vboxLayout.AddWidget(page.loginLabel, 0, core.Qt__AlignCenter)
	vboxLayout.AddSpacing(20)
	vboxLayout.AddWidget(page.tipsLabel, 1, core.Qt__AlignHCenter)
	vboxLayout.AddSpacing(100)
	vboxLayout.AddWidget(page.username, 0, core.Qt__AlignHCenter)
	vboxLayout.AddWidget(page.password, 0, core.Qt__AlignHCenter)
	vboxLayout.AddWidget(page.SkipButton, 0, core.Qt__AlignRight)
	vboxLayout.AddStretch(1)

	mainLayout.AddStretch(1)
	mainLayout.AddLayout(vboxLayout, 0)
	mainLayout.AddStretch(1)

	page.SetLayout(mainLayout)

	page.username.ConnectLeaveEvent(func(event *core.QEvent) {
		config.Conf.SetUsername(page.username.Text())
	})

	page.password.ConnectLeaveEvent(func(event *core.QEvent) {
		config.Conf.SetPassword(page.password.Text())
	})
}

func (page *AccountPage) SetTips(tips string) {
	page.tipsLabel.SetText(tips)
}

func (page *AccountPage) SetEnableLogin(enable bool) {
	page.username.SetVisible(enable)
	page.password.SetVisible(enable)
	page.SkipButton.SetVisible(enable)
}

func (page *AccountPage) LanguageChange() {
	page.loginLabel.SetText(i18n.Tr("Login"))
	page.username.SetPlaceholderText(i18n.Tr("Username"))
	page.password.SetPlaceholderText(i18n.Tr("Password"))
	page.SkipButton.SetText(i18n.Tr("Skip"))
}
