package styles

const ImageButton string = `QPushButton[flat="true"] {
  border: none;
  outline: none;
  color: #eaeaea;
}

QPushButton {
  background-color: transparent;
  border-radius: 10px;
}

QPushButton::checked {
  background-color: rgba(255, 255, 255, 0.2);
}

QPushButton::hover {
  background-color: rgba(255, 255, 255, 0.3);
}

QPushButton::pressed {
  color: #2ca7f8;
  background-color: rgba(255, 255, 255, 0.1);
}

QLabel {
	font-size:20px;
}`
