package styles

const LangListWidget string = `QListWidget {
	background: rgba(255,255,255,50);
    border: 1px solid transparent;
    border-radius: 4px;
}

QListWidget::item {
	padding: 5px;
	border:0;
}

QListWidget::item:hover {
	border-radius: 4px;
	background: rgba(0,167,255,50);
}

QListWidget::item:selected {
	border-radius: 4px;
	background: rgba(0,167,255,255);
    color:white;
}

QScrollBar {width: 2px}`
