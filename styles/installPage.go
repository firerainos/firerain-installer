package styles

const MessageTextBrowser string = `QTextBrowser {
	background: rgba(255,255,255,50);
    border: 1px solid transparent;
    border-radius: 4px;
}

QScrollBar {width: 2px}`
