package styles

const WifiListWidget string = `QListWidget {
	background: rgba(255,255,255,50);
    border: 1px solid transparent;
    border-radius: 4px;
}

QListWidget::item:hover {
	border-radius: 4px;
	background: rgba(0,167,255,50);
}

QListWidget::item:selected {
	background: transparent;
}

QScrollBar {width: 2px}`
