package main

import (
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/firerainos/firerain-installer/config"
	_ "gitlab.com/firerainos/firerain-installer/translations"
	"gitlab.com/firerainos/firerain-installer/ui"
	"gitlab.com/xiayesuifeng/go-i18n"
	"io"
	"log"
	"os"
)

func main() {
	logFile := initLogger()
	defer logFile.Close()

	config.InitConfig()

	app := widgets.NewQApplication(len(os.Args), os.Args)
	app.SetApplicationVersion("0.6.3")
	app.SetApplicationName(i18n.Tr("FireRain Installer"))
	app.SetWindowIcon(gui.NewQIcon5("/usr/share/icons/hicolor/192x192/apps/firerain-installer.svg"))

	i18n.LoadTranslator()

	ui.NewMainWindow().Show()

	os.Exit(app.Exec())
}

func initLogger() *os.File {
	logFile, err := os.OpenFile("/tmp/firerain-installer.log", os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	} else {
		log.SetOutput(io.MultiWriter(logFile, os.Stdout))
	}

	return logFile
}
