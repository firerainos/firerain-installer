module gitlab.com/firerainos/firerain-installer

go 1.16

require (
	github.com/Jguer/go-alpm/v2 v2.0.5
	github.com/Wifx/gonetworkmanager v0.2.0
	github.com/creack/pty v1.1.11
	github.com/fsnotify/fsnotify v1.4.9
	github.com/godbus/dbus/v5 v5.0.3
	github.com/hajimehoshi/oto v0.7.1
	github.com/therecipe/qt v0.0.0-20200904063919-c0c124a5770d
	github.com/tosone/minimp3 v1.0.1
	gitlab.com/xiayesuifeng/go-i18n v0.0.0-20210331070049-e1f6fbdb25c2
	gitlab.com/xiayesuifeng/go-pacman v0.0.0-20210622083626-382eb266a217
)
