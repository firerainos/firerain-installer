package config

import "os"

var Conf *Config

type Config struct {
	*Account
	*Package
	DEApplication string
	Browser       string
	InstallDev    string
	IsUEFI        bool
	EFIDev        string
	AllEFIDev     []string
	SimpleMode    bool
}

func InitConfig() {
	Conf = &Config{Account: &Account{}, Package: &Package{}, IsUEFI: true, SimpleMode: true}
	Conf.AddPackage("base")
	Conf.AddPackage("base-devel")
	Conf.AddPackage("linux")
	Conf.AddPackage("linux-firmware")
	Conf.AddPackage("vim")
	Conf.AddPackage("sudo")
	Conf.AddPackage("networkmanager")
	Conf.AddPackage("firerain-firstboot")
	Conf.AddPackage("firerain-welcome")
	Conf.AddPackage("fcitx5")
	Conf.AddPackage("fcitx5-gtk")
	Conf.AddPackage("fcitx5-qt")
	Conf.AddPackage("fcitx5-configtool")
	Conf.AddPackage("fcitx5-chinese-addons")

	Conf.DEApplication = "kde-applications"

	if _, err := os.Stat("/sys/firmware/efi/efivars"); err != nil {
		if os.IsNotExist(err) {
			Conf.IsUEFI = false
		}
	}
}

func (config *Config) SetInstallDev(dev string) {
	config.InstallDev = dev
}
func (config *Config) SetEFIDev(dev string) {
	config.EFIDev = dev
}
