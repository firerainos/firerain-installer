# firerain-installer
> firerain installer

[![pipeline status](https://gitlab.com/firerainos/firerain-installer/badges/master/pipeline.svg)](https://gitlab.com/firerainos/firerain-installer/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/firerainos/firerain-installer)](https://goreportcard.com/report/gitlab.com/firerainos/firerain-installer)
[![GoDoc](https://godoc.org/gitlab.com/firerainos/firerain-installer?status.svg)](https://godoc.org/gitlab.com/firerainos/firerain-installer)
[![Sourcegraph](https://sourcegraph.com/gitlab.com/firerainos/firerain-installer/-/badge.svg)](https://sourcegraph.com/gitlab.com/firerainos/firerain-installer)

## Dependencies
[therecipe/qt](https://github.com/therecipe/qt.git)

[fsnotify](https://github.com/fsnotify/fsnotify)

[dbus](https://github.com/godbus/dbus)

[gonetworkmanager](https://github.com/Wifx/gonetworkmanager)

[pty](https://github.com/creack/pty)

[go-alpm](https://github.com/Jguer/go-alpm)

## Install therecipe/qt

[therecipe/qt install](https://github.com/therecipe/qt/wiki/Installation)
or
[install therecipe/qt in ArchLinux](https://blog.firerain.me/article/6)

add $GOPATH/go/bin to environment variables


## Build (Go Module mode)

```bash
git clone https://gitlab.com/firerainos/firerain-installer
cd firerain-installer
qtdeploy -tags=six build desktop 
```

## License
This package is released under [GPLv3](LICENSE).