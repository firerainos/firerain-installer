package parted

import (
	"strconv"
	"strings"
)

type Partition struct {
	Device     Device
	Number     int
	Start      string
	End        string
	Size       string
	FileSystem string
	Name       string
	Flags      []string
}

func NewPartition(Device Device, Number int, Start, End, Size, FileSystem, Name string, Flags []string) Partition {
	return Partition{Device, Number, Start, End, Size, FileSystem, Name, Flags}
}

func (p *Partition) GetPath() string {
	if strings.Contains(p.Device.Disk, "nvme") {
		return p.Device.Disk + "p" + strconv.Itoa(p.Number)
	} else {
		return p.Device.Disk + strconv.Itoa(p.Number)
	}
}
