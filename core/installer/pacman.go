package installer

import (
	"bufio"
	"github.com/Jguer/go-alpm/v2"
	"github.com/creack/pty"
	"gitlab.com/firerainos/firerain-installer/config"
	"gitlab.com/xiayesuifeng/go-pacman"
	"log"
	"os/exec"
	"sync"
)

var (
	once   sync.Once
	pacMan *pacman.Pacman
)

func PacmanInstance() *pacman.Pacman {
	once.Do(func() {
		if pacMan == nil {
			pacMan, _ = pacman.NewPacman()
		}
	})

	return pacMan
}

func Pacstrap(out chan string) error {
	return installPkg(out, "-r", "/mnt", "--cachedir=/mnt/var/cache/pacman/pkg")
}

func installPkg(out chan string, arg ...string) error {
	cmdArg := append([]string{"-Sy", "--noconfirm"}, arg...)
	cmdArg = append(cmdArg, config.Conf.PkgList...)
	cmd := exec.Command("pacman", cmdArg...)
	pacman, err := pty.Start(cmd)
	if err != nil {
		return err
	}
	defer pacman.Close()

	reader := bufio.NewReaderSize(pacman, 1024)

	for {
		line, _, err := reader.ReadLine()
		if err != nil {
			break
		}

		out <- string(line)
	}

	return cmd.Wait()
}

func SyncDatabase() error {
	cmd := exec.Command("pacman", "-Syy")
	return cmd.Run()
}

func GetGroupPkgInfo(group string) []alpm.IPackage {
	pacman := PacmanInstance()

	list, err := pacman.SyncDB()
	if err != nil {
		return make([]alpm.IPackage, 0)
	}
	pkgList := list.FindGroupPkgs(group)
	return pkgList.Slice()
}

func PkgIsExist(pkgName string) bool {
	pacman := PacmanInstance()

	list, err := pacman.SyncDB()
	if err != nil {
		log.Println(err)
		return false
	}

	for _, db := range list.Slice() {
		if pkg := db.Pkg(pkgName); pkg != nil {
			return true
		}
	}
	return false
}
