package installer

import (
	"gitlab.com/firerainos/firerain-installer/api"
	"os"
	"os/exec"
)

type IpApi struct {
	Query    string `json:"query"`
	Timezone string `json:"timezone"`
}

func AutoSetTimezone() error {
	timezone, err := getTimezone()
	if err != nil {
		return err
	}

	os.Remove("/etc/localtime")
	if err := os.Symlink("/usr/share/zoneinfo/"+timezone, "/etc/localtime"); err != nil {
		return err
	}

	cmd := exec.Command("hwclock", "--systohc")
	return cmd.Run()
}

func getTimezone() (string, error) {
	ipApi, err := api.GetIpApi()
	if err != nil {
		return "", err
	} else {
		return ipApi.Timezone, nil
	}
}
