package installer

import (
	"errors"
	"gitlab.com/firerainos/firerain-installer/config"
	"os/exec"
)

func EnableServices() error {
	if err := Chroot(); err != nil {
		return err
	}
	defer ExitChroot()

	for _, pkg := range config.Conf.PkgList {
		var err error
		switch pkg {
		case "sddm":
			err = EnableService("sddm")
		case "lightdm":
			err = EnableService("lightdm")
		case "networkmanager":
			err = EnableService("NetworkManager")
		case "teamviewer":
			fallthrough
		case "teamviewer-beta":
			err = EnableService("teamviewerd")
		case "bumblebee":
			err = EnableService("bumblebeed")
		case "firerain-firstboot":
			err = EnableService("firerain-firstboot")
		case "tlp":
			if err = EnableService("tlp"); err != nil {
				break
			}
			err = EnableService("tlp-sleep")
		}
		if err != nil {
			return err
		}
	}

	return nil
}

func EnableService(service string) error {
	cmd := exec.Command("systemctl", "enable", service)
	b, err := cmd.CombinedOutput()
	if err != nil {
		return errors.New(string(b))
	}

	return nil
}
