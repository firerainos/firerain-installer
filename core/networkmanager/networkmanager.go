package networkmanager

import (
	"github.com/Wifx/gonetworkmanager"
	"github.com/godbus/dbus/v5"
	"log"
	"os/exec"
	"strings"
)

type NetworkManager struct {
	nm             gonetworkmanager.NetworkManager
	deviceWireless gonetworkmanager.DeviceWireless

	wifiList chan WifiList
}

type WifiInfo struct {
	Ssid   string
	Signal uint8
	Flags  gonetworkmanager.Nm80211APFlags
	InUse  bool
}

type WifiList []WifiInfo
type WifiListMap map[string]WifiInfo

func NewNetworkManager() *NetworkManager {
	nm, err := gonetworkmanager.NewNetworkManager()
	if err != nil {
		log.Println(err)
	}
	return &NetworkManager{nm: nm, wifiList: make(chan WifiList, 1)}
}

func (n *NetworkManager) SetWifiStatus(status bool) {
	var cmd *exec.Cmd
	if status {
		cmd = exec.Command("nmcli", "r", "wifi", "on")
	} else {
		cmd = exec.Command("nmcli", "r", "wifi", "off")
	}
	cmd.Start()
}

func (n *NetworkManager) WifiStatus() bool {
	enabled, err := n.nm.GetPropertyWirelessEnabled()
	if err != nil {
		return false
	}

	return enabled
}

func (n *NetworkManager) CheckHasWifi() bool {
	enabled, err := n.nm.GetPropertyWirelessHardwareEnabled()
	if err != nil {
		return false
	}

	if enabled {
		devices, err := n.nm.GetDevices()
		if err != nil {
			return false
		}

		for _, device := range devices {
			if deviceType, err := device.GetPropertyDeviceType(); err == nil && deviceType == gonetworkmanager.NmDeviceTypeWifi {
				if n.deviceWireless, err = gonetworkmanager.NewDeviceWireless(device.GetPath()); err == nil {
					return true
				}
			}
		}
	}

	return false
}

func (n *NetworkManager) WifiScan() {
	n.scanWifiOnce()

	signals := n.nm.Subscribe()
	go func() {
		for signal := range signals {
			if signal.Name == gonetworkmanager.DeviceWirelessInterface+".PropertiesChanged" {
				for _, body := range signal.Body {
					if value, ok := body.(map[string]dbus.Variant); ok {
						for k, v := range value {
							if strings.Contains(k, "AccessPoints") {
								tmpList := WifiListMap{}

								activeSSID := ""
								if activeAccessPoint, err := n.deviceWireless.GetPropertyActiveAccessPoint(); err == nil && activeAccessPoint != nil {
									activeSSID, _ = activeAccessPoint.GetPropertySSID()
								}

								for _, objectPath := range v.Value().([]dbus.ObjectPath) {
									if point, err := gonetworkmanager.NewAccessPoint(objectPath); err == nil {
										if ssid, err := point.GetPropertySSID(); err == nil {
											inUse := ssid == activeSSID

											if wifi, exist := tmpList[ssid]; exist {
												if strength, err := point.GetPropertyStrength(); err == nil && strength > wifi.Signal {
													wifi.Signal = strength
													if flags, err := point.GetPropertyFlags(); err == nil {
														wifi.Flags = gonetworkmanager.Nm80211APFlags(flags)
														wifi.InUse = inUse
													}
												}
											} else if strength, err := point.GetPropertyStrength(); err == nil {
												if flags, err := point.GetPropertyFlags(); err == nil {
													tmpList[ssid] = WifiInfo{Ssid: ssid, Signal: strength, Flags: gonetworkmanager.Nm80211APFlags(flags), InUse: inUse}
												}
											}
										}
									}
								}

								n.wifiList <- n.sortWifiList(tmpList)
							}
						}
					}
				}
			}
		}
	}()
}

func (n *NetworkManager) scanWifiOnce() {
	if points, err := n.deviceWireless.GetAllAccessPoints(); err == nil {
		tmpList := WifiListMap{}

		activeSSID := ""
		if activeAccessPoint, err := n.deviceWireless.GetPropertyActiveAccessPoint(); err == nil && activeAccessPoint != nil {
			activeSSID, _ = activeAccessPoint.GetPropertySSID()
		}

		for _, point := range points {
			if ssid, err := point.GetPropertySSID(); err == nil {
				inUse := ssid == activeSSID

				if wifi, exist := tmpList[ssid]; exist {
					if strength, err := point.GetPropertyStrength(); err == nil && strength > wifi.Signal {
						wifi.Signal = strength
						if flags, err := point.GetPropertyFlags(); err == nil {
							wifi.Flags = gonetworkmanager.Nm80211APFlags(flags)
							wifi.InUse = inUse
						}
					}
				} else if strength, err := point.GetPropertyStrength(); err == nil {
					if flags, err := point.GetPropertyFlags(); err == nil {
						tmpList[ssid] = WifiInfo{Ssid: ssid, Signal: strength, Flags: gonetworkmanager.Nm80211APFlags(flags), InUse: inUse}
					}
				}
			}
		}

		n.wifiList <- n.sortWifiList(tmpList)
	}
}

func (n *NetworkManager) sortWifiList(wifiList WifiListMap) WifiList {
	list := make([]WifiInfo, 0)

	for _, info := range wifiList {
		length := len(list)

		isAppend := false
		for i := 0; i < length; i++ {
			if info.Signal > list[i].Signal {
				list = append(list, list[length-1])
				for j := length - 1; j > i; j-- {
					list[j] = list[j-1]
				}
				list[i] = info
				isAppend = true
				break
			}
		}

		if !isAppend {
			list = append(list, info)
		}
	}

	return list
}

func (n *NetworkManager) StopWifiScan() {
	n.nm.Unsubscribe()
}

func (n *NetworkManager) WifiList() chan WifiList {
	return n.wifiList
}

func (n *NetworkManager) ConnectWifi(ssid, password string) {
	var cmd *exec.Cmd
	if password == "" {
		cmd = exec.Command("nmcli", "-t", "dev", "wifi", "connect", ssid)
	} else {
		cmd = exec.Command("nmcli", "-t", "dev", "wifi", "connect", ssid, "password", password)
	}
	cmd.Run()
}
